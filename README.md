ics-ans-role-pxe-boot
===================

Configure PXE Boot.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

- ...

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: ics-ans-role-pxe-boot }
```

License
-------

BSD 2-clause
